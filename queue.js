
let collection = [];
// Write the queue functions below.



function enqueue(element) {
    collection.push(element);
    return this.collection;
}

function print() {
    return this.collection;
}

function dequeue() {
    collection.shift();
    return this.collection;
}

function front() {
    if(this.collection.length > 0){
        return this.collection[0];
    }
}

function size() {
    return this.collection.length;
}

function isEmpty() {
   if(this.collection.length > 0){
        return false;
    } else {
        return true;
    }
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
